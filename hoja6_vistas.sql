﻿USE m1u2h6;

/*
  APARTADO 1:
*/

  CREATE OR REPLACE VIEW CONSULTA1 AS
    SELECT 
        c.`CÓDIGO CLIENTE`, p.`NÚMERO DE PEDIDO`, c.EMPRESA, c.POBLACIÓN
      FROM clientes c  
      JOIN pedidos p 
      USING (`CÓDIGO CLIENTE`)
      WHERE c.POBLACIÓN='Madrid' OR c.POBLACIÓN='Barcelona' OR c.POBLACIÓN='Zaragoza';

  SELECT * FROM CONSULTA1 c;


/*
    APARTADO 2:
*/
   
-- Consulta C1
SELECT 
  c.`CÓDIGO CLIENTE`, c.EMPRESA, c.POBLACIÓN 
FROM clientes c 
WHERE c.POBLACIÓN IN ('Madrid','Barcelona','Zaragoza');

-- Consulta completa
SELECT 
      c1.`CÓDIGO CLIENTE`, c1.EMPRESA, c1.POBLACIÓN, p.`NÚMERO DE PEDIDO` 
    FROM 
    (
      SELECT c.`CÓDIGO CLIENTE`, c.EMPRESA, c.POBLACIÓN FROM clientes c WHERE c.POBLACIÓN IN ('Madrid','Barcelona','Zaragoza')
    ) c1
    JOIN 
    pedidos p
    USING (`CÓDIGO CLIENTE`);


/*
  APARTADO 3:
  */

CREATE OR REPLACE VIEW CONSULTA1OPTIMIZADA AS 
  SELECT 
      c1.`CÓDIGO CLIENTE`, c1.EMPRESA, c1.POBLACIÓN, p.`NÚMERO DE PEDIDO` 
    FROM 
    (
     SELECT 
       c.`CÓDIGO CLIENTE`, c.EMPRESA, c.POBLACIÓN 
     FROM clientes c 
     WHERE c.POBLACIÓN IN ('Madrid','Barcelona','Zaragoza')
    ) c1
    JOIN 
    pedidos p
    USING (`CÓDIGO CLIENTE`);




/*
  APARTADO 4:
*/

  -- apartado a
  -- consulta C1 --> VISTA: Subconsulta1Consulta1Optimizada

  CREATE OR REPLACE VIEW Subconsulta1Consulta1Optimizada AS
    SELECT 
      c.`CÓDIGO CLIENTE`, c.EMPRESA, c.POBLACIÓN 
    FROM clientes c 
    WHERE c.POBLACIÓN IN ('Madrid','Barcelona','Zaragoza');

  -- apartado b
  
CREATE OR REPLACE VIEW CONSULTA1OPTIMIZADA AS 
  SELECT 
      sco.`CÓDIGO CLIENTE`, sco.EMPRESA, sco.POBLACIÓN, p.`NÚMERO DE PEDIDO` 
    FROM  pedidos p
    JOIN Subconsulta1Consulta1Optimizada sco
    USING (`CÓDIGO CLIENTE`);

SELECT * FROM CONSULTA1OPTIMIZADA co;


/*
  APARTADO 5:
*/

  -- subconsulta1consulta2optimizada:
  
  SELECT 
    p.`NÚMERO DE PEDIDO`,p.`FECHA DE PEDIDO`,p.`FORMA DE PAGO` 
  FROM pedidos p 
  WHERE year(p.`FECHA DE PEDIDO`)='2002' 
  AND p.`FORMA DE PAGO`='TARJETA';


  -- consulta2Optimizada
    SELECT
      c.EMPRESA,c.POBLACIÓN, s.`NÚMERO DE PEDIDO`, s.`FECHA DE PEDIDO`,s.`FORMA DE PAGO` 
    FROM clientes c 
    JOIN 
      (
       SELECT 
        p.`CÓDIGO CLIENTE`,p.`NÚMERO DE PEDIDO`,p.`FECHA DE PEDIDO`,p.`FORMA DE PAGO` 
       FROM pedidos p 
       WHERE year(p.`FECHA DE PEDIDO`)='2002' 
       AND p.`FORMA DE PAGO`='TARJETA'
      ) s
    USING(`CÓDIGO CLIENTE`)
    ORDER BY s.`FECHA DE PEDIDO`;


/*
  APARTADO 6:
*/

  -- VISTAS:
  
  CREATE OR REPLACE VIEW subconsulta1consulta2optimizada AS  
    SELECT 
      p.`CÓDIGO CLIENTE`,p.`NÚMERO DE PEDIDO`,p.`FECHA DE PEDIDO`,p.`FORMA DE PAGO` 
    FROM pedidos p 
    WHERE year(p.`FECHA DE PEDIDO`)='2002' 
    AND p.`FORMA DE PAGO`='TARJETA';


  CREATE OR REPLACE VIEW consulta2Optimizada AS 
    SELECT
      c.EMPRESA,c.POBLACIÓN, s.`NÚMERO DE PEDIDO`, s.`FECHA DE PEDIDO`,s.`FORMA DE PAGO` 
    FROM clientes c 
    JOIN subconsulta1consulta2optimizada s
    USING(`CÓDIGO CLIENTE`)
    ORDER BY s.`FECHA DE PEDIDO`;

  SELECT * FROM consulta2Optimizada o;


  /*
    APARTADO 7:
  */

    -- subconsultas y consultas

    -- c1: calculo numero de pedidos por cliente
    SELECT `CÓDIGO CLIENTE`, COUNT(*) nPedidos FROM pedidos GROUP BY `CÓDIGO CLIENTE`;

    -- c2: maximo del numero de pedidos
    SELECT MAX(c1.nPedidos) maximo FROM (
      SELECT `CÓDIGO CLIENTE`, COUNT(*) nPedidos FROM pedidos GROUP BY `CÓDIGO CLIENTE`
      ) c1 ;

    -- c3: obtengo quien es el mayor igualando los numeros
    SELECT c1.`CÓDIGO CLIENTE` FROM 
      (SELECT `CÓDIGO CLIENTE`, COUNT(*) nPedidos FROM pedidos GROUP BY `CÓDIGO CLIENTE`)
      c1 JOIN 
      (
      SELECT MAX(c1.nPedidos) maximo FROM (
      SELECT `CÓDIGO CLIENTE`, COUNT(*) nPedidos FROM pedidos GROUP BY `CÓDIGO CLIENTE`
      ) c1 ) c2
      ON c1.nPedidos=c2.maximo;

    -- final

    SELECT c.`CÓDIGO CLIENTE`,c.TELÉFONO FROM (
       SELECT c1.`CÓDIGO CLIENTE` FROM 
      (SELECT `CÓDIGO CLIENTE`, COUNT(*) nPedidos FROM pedidos GROUP BY `CÓDIGO CLIENTE`)
      c1 JOIN 
      (
      SELECT MAX(c1.nPedidos) maximo FROM (
      SELECT `CÓDIGO CLIENTE`, COUNT(*) nPedidos FROM pedidos GROUP BY `CÓDIGO CLIENTE`
      ) c1 ) c2
      ON c1.nPedidos=c2.maximo) c3
      JOIN clientes c USING (`CÓDIGO CLIENTE`);


    -- VISTAS
      CREATE OR REPLACE VIEW subc1consulta3 AS
        SELECT 
          `CÓDIGO CLIENTE`, COUNT(*) nPedidos
        FROM pedidos 
        GROUP BY `CÓDIGO CLIENTE`;


      CREATE OR REPLACE VIEW subc2consulta3 AS
          SELECT
            MAX(s.nPedidos) maximo 
          FROM subc1consulta3 s;

      CREATE OR REPLACE VIEW subc3consulta3 AS 
        SELECT s.`CÓDIGO CLIENTE` 
          FROM subc1consulta3 s 
          JOIN subc2consulta3 s1
          ON s.nPedidos=s1.maximo;

      CREATE OR REPLACE VIEW consulta3 AS
        SELECT 
          c.`CÓDIGO CLIENTE`,c.TELÉFONO 
        FROM subc3consulta3 s
        JOIN clientes c 
        USING (`CÓDIGO CLIENTE`);

      SELECT * FROM consulta3 c;


  
